# Chef

Simple repository to learn Chef.io

## Pre-requisites

* Chef Workstation
* VirtualBox
* Vagrant

## Bootstrap the cookbook

```bash
chef generate cookbook learn_chef
```

## Creating environment

```bash
cd learn_chef
kitchen create
```

## List kitchen environments

```bash
kitchen list
```

## Machine Login

From learn_chef directory:

```bash
kitchen login centos
kitchen login ubuntu
```

## Execute recipes

From learn_chef directory:

```bash
kitchen converge
```

## Discover metadata about the node to use in recipes

```bash
kitchen login ubuntu
ohai
```

## How to clean the environment

From learn_chef directory:

```bash
kitchen destroy
```